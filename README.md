# Feedbacking

Supercharge feedback exchange.

Live Demo https://dev.dreamorosi.com/feedbacking/

*Contains*

* Landing page (index)
* Contact us
* Login page
* Dashboard

## Install

```sh
git clone https://bitbucket.org/dreamorosi/feedbacking.git

cd feedbacking

yarn install | npm install
```

## Develop

Starts Sass watcher

```sh
yarn start | npm start
```

## Demo

Some pages have demo options that can be toggled from the console. A message will appear in the console if the feature is available on the page.

## ToDo
- [] Finish form module
- [] Create demo module
- [] Find a new Carousel without jQuery
- [] Remove jQuery

**An internet connection is currently required as some placeholder images are loaded from http://placehold.it and the fonts are loaded from Google Fonts**
