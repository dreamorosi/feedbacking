/* global fetch sessionStorage */

function init () {
  let loginForm = document.querySelector('#loginForm')
  loginForm.onsubmit = submitForm
  demo(loginForm)
}

document.onload = init()

function formMapsFactory (form) {
  if (!form.elements) {
    throw (Error('Wrong form'))
  }

  let formElements = Array.from(form.elements)
    .filter(el => el.tagName !== 'BUTTON')
  let formMap = new Map()
  formElements.forEach(el => formMap.set(el.name, el))
  formMap.set('getJson', () => {
    let obj = {}
    formMap.forEach((value, key) => {
      if (typeof value !== 'function') {
        obj[key] = value.value
      }
    })
    return obj
  })
  return formMap
}

function submitForm (e) {
  e.preventDefault()
  let form = formMapsFactory(e.target)
  let formData = form.get('getJson')()
  demoFetch(formData)
    // .then(rawResponse => rawResponse.json())
    .then(res => {
      if (res.isAuth) {
        window.location = './dashboard.html'
      } else if (res.error) {
        let { errorFields } = res.error
        if (errorFields.length) {
          errorFields.forEach(fieldName => {
            if (form.has(fieldName)) {
              form.get(fieldName).classList.add('is-invalid')
            }
          })
        }
      }
      console.log(res)
      // storeUserData(res.user)
    })
    .catch(err => console.log(err))
}

function cacheUser (userData) {
  sessionStorage.setItem('uuid', userData.uuid)
  sessionStorage.setItem('fullName', userData.fullName)
  sessionStorage.setItem('email', userData.email)
  sessionStorage.setItem('plan', userData.plan)
  sessionStorage.setItem('address', JSON.stringify(userData.address))
  sessionStorage.setItem('projects', JSON.stringify(userData.projects))
}

// Activate demo elements
function demo (loginForm) {
  window.demoCode = 2
  console.log(`Website in Demo mode : ${window.demoCode}`)
  console.log(`Mode list:\n1. Successfull login\n2. Wrong email\n3. Wrong password\n4. Random error\n\nTo change mode type "window.demoCode = $NUMBER"`)

  fetch('./data/users.json')
    .then(rawResponse => rawResponse.json())
    .then(user => {
      let [emailInput, passInput] = Array.from(loginForm.elements).slice(0, 2)

      let fillInBtn = document.querySelector('.btn-outline-danger')
      fillInBtn.classList.toggle('d-none')
      fillInBtn.onclick = function () {
        emailInput.value = user.email
        passInput.value = user.password
      }
      cacheUser(user)
    })
    .catch(err => console.error(err))
}

function demoFetch (data) {
  const response = {
    'isAuth': true,
    'error': false
  }

  if (window.demoCode === 1) {
    return Promise.resolve(response)
  } else {
    response.isAuth = false
    if (window.demoCode === 2) {
      response.error = {
        'text': 'Wrong email',
        'code': 1,
        'errorFields': ['email']
      }
    } else if (window.demoCode === 3) {
      response.error = {
        'text': 'Wrong password',
        'code': 2,
        'errorFields': ['password']
      }
    } else {
      response.error = {
        'text': 'Random Error',
        'code': '36',
        'errorFields': ['email']
      }
    }
    return fetch('http://www.winelove.club/doc/html/testRestApi.php', {
      method: 'post',
      body: JSON.stringify({obj: response})
    })
    .then(res => res.json())
    .then(function (data) {
      console.log(data)
      return Promise.resolve(data)
    })
    // return Promise.resolve(response)
    // return Promise.reject(Error('Hola!'))
  }
}
