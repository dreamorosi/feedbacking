/* global sessionStorage */
function init () {
  if (sessionStorage.getItem('uuid')) {
    let nameLabel = document.querySelector('#userName')
    let emailLabel = document.querySelector('.email')
    let planLabel = document.querySelector('.plan .btn')
    let addressLabel = document.querySelector('.address')

    nameLabel.innerText = sessionStorage.getItem('fullName')
    emailLabel.innerText = sessionStorage.getItem('email')
    if (sessionStorage.getItem('plan') === '0') {
      planLabel.innerText = 'FREE'
    } else {
      planLabel.innerText = 'STANDARD'
    }
    let address = JSON.parse(sessionStorage.getItem('address'))
    addressLabel.innerText = address.streetB + ', ' + address.state
  } else {
    window.location = './login.html'
  }

  let logoutBtn = document.querySelector('a[title="Logout"]')
  logoutBtn.onclick = function (e) {
    e.preventDefault()
    flushUserData()
    window.location = './'
  }
}

document.onload = init()

function flushUserData () {
  sessionStorage.removeItem('uuid')
  sessionStorage.removeItem('fullName')
  sessionStorage.removeItem('email')
  sessionStorage.removeItem('plan')
  sessionStorage.removeItem('address')
  sessionStorage.removeItem('projects')
}
