/* global $ */

$(document).ready(function () {
  $('.form-inline').on('submit', function (e) {
    e.preventDefault()
    let form = formMapsFactory($(this)[0])
    let jsonContent = form.get('getJson')()
    let $btn = $(this).find('button[type="submit"]')
    $btn.attr('disabled', true)
    $btn.text('Loading')

    var settings = {
      'async': true,
      'crossDomain': true,
      'url': 'http://www.winelove.club/doc/html/testRestApi.php',
      'method': 'POST',
      'headers': {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      'data': { obj: JSON.stringify(jsonContent) }
    }

    $.ajax(settings)
      .done((response) => {
        console.log(response)
        $btn.attr('disabled', false)
        $btn.text('Subscribe')
        $('section.newsletter .row').empty()
        $('section.newsletter .row:first-child')
          .append('<div class="col text-center"><h2>Thank You</h2></div>')
          .fadeIn()
      })
  })
})

function formMapsFactory (form) {
  if (!form.elements) {
    throw (Error('Wrong form'))
  }

  let formElements = Array.from(form.elements)
    .filter(el => el.tagName !== 'BUTTON')
  let formMap = new Map()
  formElements.forEach(el => formMap.set(el.name, el))
  formMap.set('getJson', () => {
    let obj = {}
    formMap.forEach((value, key) => {
      if (typeof value !== 'function') {
        obj[key] = value.value
      }
    })
    return obj
  })
  return formMap
}
