/* global fetch */
function init () {
  loadCountries()
}

document.onload = init()

function loadCountries () {
  var select = document.querySelector('select[name="country"]')

  fetch('./data/countries.json')
    .then(res => {
      return res.json()
    })
    .then(countriesGroups => {
      let options = Object.keys(countriesGroups).map(groupLabel => {
        let countriesOpts = countriesGroups[groupLabel]
          .map(country => `<option value="${country.value}">${country.name}</option>`)

        return `<optgroup label="${groupLabel}">${countriesOpts}</optgroup>`
      })

      select.innerHTML += options
    })
    .catch(err => {
      console.error(err)
    })
}
